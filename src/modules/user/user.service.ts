import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm'

import { Repository } from 'typeorm'

import { User } from './user.entity'

@Injectable()
export class UserService extends TypeOrmQueryService<User> {
  constructor(@InjectRepository(User) userRepository: Repository<User>) {
    super(userRepository, { useSoftDelete: true })
  }
}
