import { HideField } from '@nestjs/graphql'

import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { compareSync, genSalt, hash } from 'bcrypt'
import { GraphQLError } from 'graphql'

import { Group } from '../group/group.entity'
import { Issue } from '../issue/issue.entity'

@Entity()
export class User {
  @HideField()
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ default: true })
  active: boolean

  @HideField()
  @ManyToMany(
    type => Group,
    group => group.users,
  )
  groups?: Group[]

  @HideField()
  @ManyToMany(
    type => Issue,
    issue => issue.users,
  )
  issues?: Issue[]

  @Column({ unique: true })
  email!: string

  @Column()
  name!: string

  @Column()
  @HideField()
  password!: string

  confirmPassword!: string | null

  @CreateDateColumn()
  createdAt?: Date | null

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date | null

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date | null

  @HideField()
  private tempPassword: string

  @AfterLoad()
  private loadTempPassword(): void {
    this.tempPassword = this.password
  }

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword(): Promise<void> {
    if (this.confirmPassword !== this.password) throw new GraphQLError('Senhas estão diferentes!')
    if (this.tempPassword !== this.password) {
      this.password = await hash(this.password, await genSalt(10))
      this.loadTempPassword()
    }
  }

  passwordMatches(password: string): boolean {
    return compareSync(password, this.password)
  }
}
