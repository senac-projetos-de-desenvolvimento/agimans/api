import { Connection, FilterableField } from '@nestjs-query/query-graphql'
import { Field, GraphQLISODateTime, HideField, Int, ObjectType } from '@nestjs/graphql'

import { Group } from '../group/group.entity'
import { GroupDTO } from '../group/group.dto'
import { Issue } from '../issue/issue.entity'
import { IssueDTO } from '../issue/issue.dto'

@Connection('group', () => GroupDTO, { nullable: true })
@Connection('issue', () => IssueDTO, { nullable: true })
@ObjectType('User')
export class UserDTO {
  @FilterableField(() => Int)
  id!: number

  @FilterableField()
  active!: boolean

  @HideField()
  groups?: Group[]

  @HideField()
  issues?: Issue[]

  @FilterableField()
  email!: string

  @FilterableField()
  name!: string

  @Field()
  password!: string

  @Field()
  confirmPassword!: string

  @Field(() => GraphQLISODateTime)
  createdAt!: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  updatedAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date | null
}
