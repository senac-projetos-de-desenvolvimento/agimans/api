import { Module } from '@nestjs/common'
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql'
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'

import { Issue } from './issue.entity'
import { IssueDTO } from './issue.dto'
import { IssueService } from './issue.service'

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Issue])],
      resolvers: [{ DTOClass: IssueDTO, EntityClass: Issue }],
    }),
  ],
  providers: [IssueService],
})
export class IssueModule {}
