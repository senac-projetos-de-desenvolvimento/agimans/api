import { Connection, FilterableField, Relation } from '@nestjs-query/query-graphql'
import { Field, GraphQLISODateTime, HideField, Int, ObjectType } from '@nestjs/graphql'

import { Group } from '../group/group.entity'
import { GroupDTO } from './../group/group.dto'
import { List } from '../list/list.entity'
import { ListDTO } from '../list/list.dto'
import { Sprint } from '../sprint/sprint.entity'
import { SprintDTO } from '../sprint/sprint.dto'
import { Status } from '../status/status.entity'
import { StatusDTO } from '../status/status.dto'
import { User } from '../user/user.entity'
import { UserDTO } from '../user/user.dto'

@Connection('user', () => UserDTO, { nullable: true })
@ObjectType('Issue')
@Relation('group', () => GroupDTO, { nullable: true })
@Relation('list', () => ListDTO, { nullable: true })
@Relation('sprint', () => SprintDTO, { nullable: true })
@Relation('status', () => StatusDTO, { nullable: true })
export class IssueDTO {
  @FilterableField(() => Int)
  id!: number

  @FilterableField()
  active!: boolean

  @FilterableField()
  index!: number

  @HideField()
  group?: Group

  @HideField()
  list?: List

  @HideField()
  sprint?: Sprint

  @HideField()
  status?: Status

  @HideField()
  users?: User[]

  @FilterableField()
  title: string

  @Field()
  description?: string

  @Field({ nullable: true })
  estimate?: number

  @Field({ nullable: true })
  spend?: number

  @Field({ nullable: true })
  weight?: number

  @Field(() => GraphQLISODateTime)
  createdAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  updatedAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date | null
}
