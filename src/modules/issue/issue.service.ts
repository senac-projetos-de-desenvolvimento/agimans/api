import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm'

import { Repository } from 'typeorm'

import { Issue } from './issue.entity'

@Injectable()
export class IssueService extends TypeOrmQueryService<Issue> {
  constructor(@InjectRepository(Issue) issueRepository: Repository<Issue>) {
    super(issueRepository, { useSoftDelete: true })
  }
}
