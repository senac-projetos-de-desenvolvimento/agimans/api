import { Connection, FilterableField, Relation } from '@nestjs-query/query-graphql'
import { Field, GraphQLISODateTime, HideField, Int, ObjectType } from '@nestjs/graphql'

import { Group } from './group.entity'
import { Issue } from '../issue/issue.entity'
import { IssueDTO } from '../issue/issue.dto'
import { Sprint } from '../sprint/sprint.entity'
import { SprintDTO } from '../sprint/sprint.dto'
import { Status } from '../status/status.entity'
import { StatusDTO } from '../status/status.dto'
import { User } from '../user/user.entity'
import { UserDTO } from '../user/user.dto'

@Connection('childGroup', () => GroupDTO, { nullable: true })
@Connection('issue', () => IssueDTO, { nullable: true })
@Connection('sprint', () => SprintDTO, { nullable: true })
@Connection('statuses', () => StatusDTO, { nullable: true })
@Connection('user', () => UserDTO, { nullable: true })
@ObjectType('Group')
@Relation('fatherGroup', () => GroupDTO, { nullable: true })
export class GroupDTO {
  @FilterableField(() => Int)
  id!: number

  @FilterableField()
  active!: boolean

  @HideField()
  childGroups?: Group[]

  @HideField()
  fatherGroup?: Group

  @HideField()
  issues?: Issue[]

  @HideField()
  sprints?: Sprint[]

  @HideField()
  statuses?: Status[]

  @HideField()
  users?: User[]

  @FilterableField()
  project!: boolean

  @FilterableField()
  name!: string

  @FilterableField()
  slug!: string | null

  @Field()
  description?: string | null

  @Field(() => GraphQLISODateTime)
  createdAt!: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  updatedAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date | null
}
