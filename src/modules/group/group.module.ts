import { Module } from '@nestjs/common'
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql'
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'

import { Group } from './group.entity'
import { GroupDTO } from './group.dto'
import { GroupService } from './group.service'

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Group])],
      resolvers: [{ DTOClass: GroupDTO, EntityClass: Group }],
    }),
  ],
  providers: [GroupService],
})
export class GroupModule {}
