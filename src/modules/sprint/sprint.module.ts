import { Module } from '@nestjs/common'
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql'
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'

import { Sprint } from './sprint.entity'
import { SprintDTO } from './sprint.dto'
import { SprintService } from './sprint.service'

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Sprint])],
      resolvers: [{ DTOClass: SprintDTO, EntityClass: Sprint }],
    }),
  ],
  providers: [SprintService],
})
export class SprintModule {}
