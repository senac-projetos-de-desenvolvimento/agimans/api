import { HideField } from '@nestjs/graphql'

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

import { Group } from '../group/group.entity'
import { Issue } from '../issue/issue.entity'
import { List } from '../list/list.entity'

@Entity()
export class Sprint {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ default: true })
  active: boolean

  @Column()
  @Generated('increment')
  index!: number

  @HideField()
  @JoinColumn()
  @ManyToOne(
    type => Group,
    group => group.sprints,
  )
  group?: Group

  @HideField()
  @OneToMany(
    type => Issue,
    issue => issue.sprint,
  )
  issues?: Issue[]

  @HideField()
  @OneToMany(
    type => List,
    list => list.sprint,
  )
  lists?: List[]

  @Column()
  endDate: Date

  @Column()
  startDate: Date

  @Column()
  title: string

  @Column()
  description?: string

  @Column({ nullable: true })
  estimate?: number

  @Column({ nullable: true })
  spend?: number

  @Column({ nullable: true })
  weight?: number

  @CreateDateColumn()
  createdAt?: Date | null

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date | null

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date | null
}
