import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm'

import { Repository } from 'typeorm'

import { Sprint } from './sprint.entity'

@Injectable()
export class SprintService extends TypeOrmQueryService<Sprint> {
  constructor(@InjectRepository(Sprint) sprintRepository: Repository<Sprint>) {
    super(sprintRepository, { useSoftDelete: true })
  }
}
