import { HideField } from '@nestjs/graphql'

import {
  Entity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

import { Group } from '../group/group.entity'
import { Issue } from '../issue/issue.entity'
import { List } from '../list/list.entity'

@Entity()
export class Status {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ default: true })
  active: boolean

  @HideField()
  @ManyToOne(
    type => Group,
    project => project.statuses,
  )
  project?: Group

  @HideField()
  @OneToMany(
    type => Issue,
    issue => issue.status,
  )
  issues?: Issue[]

  @HideField()
  @OneToMany(
    type => List,
    list => list.status,
  )
  lists?: List[]

  @Column()
  color: string

  @Column()
  name: string

  @CreateDateColumn()
  createdAt?: Date | null

  @UpdateDateColumn({ nullable: true })
  updatedAt?: Date | null

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date | null
}
