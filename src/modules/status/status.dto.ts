import { Connection, FilterableField, Relation } from '@nestjs-query/query-graphql'
import { Field, HideField, GraphQLISODateTime, Int, ObjectType } from '@nestjs/graphql'

import { GroupDTO } from '../group/group.dto'
import { Issue } from '../issue/issue.entity'
import { IssueDTO } from '../issue/issue.dto'
import { List } from '../list/list.entity'
import { ListDTO } from '../list/list.dto'
import { Group } from '../group/group.entity'

@Connection('issue', () => IssueDTO, { nullable: true })
@Connection('list', () => ListDTO, { nullable: true })
@Connection('list', () => ListDTO, { nullable: true })
@ObjectType('Status')
@Relation('project', () => GroupDTO, { nullable: true })
export class StatusDTO {
  @FilterableField(() => Int)
  id!: number

  @FilterableField()
  active!: boolean

  @HideField()
  project?: Group

  @HideField()
  issues?: Issue[]

  @HideField()
  lists?: List

  @Field()
  color: string

  @Field()
  name: string

  @Field(() => GraphQLISODateTime)
  createdAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  updatedAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date | null
}
