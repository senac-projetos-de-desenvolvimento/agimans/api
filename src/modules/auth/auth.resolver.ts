import { Args, Mutation, Resolver } from '@nestjs/graphql'

import { AuthDTO } from './auth.dto'
import { AuthService } from './auth.service'

@Resolver('Auth')
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => AuthDTO)
  async login(@Args('email') email: string, @Args('password') password: string): Promise<AuthDTO> {
    return this.authService.login({
      email,
      password,
    })
  }
}
