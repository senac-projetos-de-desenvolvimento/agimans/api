import { Connection, FilterableField, Relation } from '@nestjs-query/query-graphql'
import { Field, GraphQLISODateTime, HideField, Int, ObjectType } from '@nestjs/graphql'

import { Issue } from '../issue/issue.entity'
import { IssueDTO } from '../issue/issue.dto'
import { Sprint } from '../sprint/sprint.entity'
import { SprintDTO } from '../sprint/sprint.dto'
import { Status } from '../status/status.entity'
import { StatusDTO } from '../status/status.dto'

@Connection('issue', () => IssueDTO, { nullable: true })
@ObjectType('List')
@Relation('sprint', () => SprintDTO, { nullable: true })
@Relation('status', () => StatusDTO, { nullable: true })
export class ListDTO {
  @FilterableField(() => Int)
  id!: number

  @FilterableField()
  active!: boolean

  @FilterableField()
  index!: number

  @HideField()
  issues?: Issue[]

  @HideField()
  sprint?: Sprint

  @HideField()
  status?: Status

  @FilterableField()
  name: string

  @Field()
  description?: string

  @Field(() => GraphQLISODateTime)
  createdAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  updatedAt?: Date | null

  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date | null
}
