import { Module } from '@nestjs/common'
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql'
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'

import { List } from './list.entity'
import { ListDTO } from './list.dto'
import { ListService } from './list.service'

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([List])],
      resolvers: [{ DTOClass: ListDTO, EntityClass: List }],
    }),
  ],
  providers: [ListService],
})
export class ListModule {}
